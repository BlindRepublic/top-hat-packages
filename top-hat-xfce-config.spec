
Name:		top-hat-xfce-config
Version:	0.6.0
Release:	1%{?dist}
Summary:	Settings for Top Hat XFCE
License:	GPL
URL:		https://gitlab.com/CheeseEBoi/top-hat-xfce-config
Source0:	%{url}/-/archive/%{version}/top-hat-xfce-config-%{version}.tar.gz

Buildarch:	noarch

# Based on configuration files from...
Requires:	top-hat-gtk-config

# Applications
Requires:	xfce4-panel-profiles 
Requires:	alacarte
Suggests:	catfish
Suggests:	xcape

# All external plugins used
Requires:	xfce4-clipman-plugin
Requires:	xfce4-cpugraph-plugin
Requires:	xfce4-places-plugin
Requires:	xfce4-xkb-plugin
Requires:	xfce4-whiskermenu-plugin

# Theming
Requires: 	greybird-dark-theme
Requires: 	elementary-xfce-icon-theme
Requires:	breeze-cursor-theme
Requires: 	fedora-workstation-backgrounds

%description
Configuration files for Top Hat Linux with the XFCE desktop


%prep
%autosetup

%install
%{make_install} PREFIX=/usr

%files
/%{_sysconfdir}/skel/.config/xfce4/
/%{_sysconfdir}/skel/.config/autostart/xcape.desktop
/%{_sysconfdir}/skel/.config/catfish/catfish.rc
/%{_sysconfdir}/skel/.config/xfce4/terminal/terminalrc

# Panel profiles
/%{_datadir}/xfce4-panel-profiles/layouts/Top-Hat.tar.bz2
/%{_datadir}/xfce4-panel-profiles/layouts/Top-Hat-Minimal.tar.bz2

%changelog
* Thu Jul 23 2020 Elliot L <CheeseEBoi@mailo.com> - 0.6.0-1
- Added proper menu editor and set basic terminal configurations

* Sun Jul 19 2020 Elliot L <CheeseEBoi@mailo.com> - 0.5.0-1
- Fixed typo in catfish.rc

* Sat Jul 18 2020 Elliot L <CheeseEBoi@mailo.com> - 0.4.1-3
- Added top-hat-gtk-config dependency

* Wed Jul 15 2020 Elliot L <CheeseEBoi@mailo.com> - 0.4
- Fixed bug with the directory menu and added necessary dependencies

* Tue Jul 14 2020 Elliot L <CheeseEBoi@mailo.com> - 0.3
- Added default background, catfish config, and customized whiskermenu
